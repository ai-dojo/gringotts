extern crate proc_macro;

use proc_macro::TokenStream;
use quote::{format_ident, quote};

struct TypeDuo {
    one:  syn::Ident,
    _sep: syn::Token![::],
    two:  syn::Ident,
}

impl syn::parse::Parse for TypeDuo {
    fn parse(input: syn::parse::ParseStream) -> syn::Result<Self> {
        Ok(Self {
            one:  input.parse()?,
            _sep: input.parse()?,
            two:  input.parse()?,
        })
    }
}

struct TypeTriplet {
    one:   syn::Ident,
    _sep:  syn::Token![::],
    two:   syn::Ident,
    _sep2: syn::Token![::],
    three: syn::Ident,
}

impl syn::parse::Parse for TypeTriplet {
    fn parse(input: syn::parse::ParseStream) -> syn::Result<Self> {
        Ok(Self {
            one:   input.parse()?,
            _sep:  input.parse()?,
            two:   input.parse()?,
            _sep2: input.parse()?,
            three: input.parse()?,
        })
    }
}

#[proc_macro]
pub fn impl_pk_stream(input: TokenStream) -> TokenStream {
    let input_struct = syn::parse_macro_input!(input as TypeDuo);

    let entity = &input_struct.one;

    let postfix = &input_struct.two;

    let func_name = format_ident!("stream_pks_{}", postfix);

    let gen = quote! {
        pub async fn #func_name(
            conn: Arc<DatabaseConnection>,
        ) -> BoxStream<'static, GringottsResult<i32>> {
            Box::pin(stream! {
            let mut db_stream  = #entity::find()
                .select_only()
                .column(services::Column::Id)
                .into_model::<PrimaryKeyView>()
                .stream(conn.as_ref())
                .await?;

                while let Some(item) = db_stream.try_next().await? {
                    yield Ok(item.id)
                }
            })
        }
    };

    gen.into()
}

#[proc_macro]
pub fn impl_pk_get(input: TokenStream) -> TokenStream {
    let input_struct = syn::parse_macro_input!(input as TypeTriplet);

    let entity = &input_struct.one;

    let model = &input_struct.two;

    let postfix = &input_struct.three;

    let func_name = format_ident!("get_{}_by_pk", postfix);

    let gen = quote! {
        pub async fn #func_name(
            pk: i32,
            conn: Arc<DatabaseConnection>,
        ) -> GringottsResult<Option<#model>>  {
            Ok(#entity::find_by_id(pk)
            .one(conn.as_ref())
            .await?)
        }
    };

    gen.into()
}

#[proc_macro]
pub fn impl_get_count(input: TokenStream) -> TokenStream {
    let input_struct = syn::parse_macro_input!(input as TypeDuo);

    let entity = &input_struct.one;

    let postfix = &input_struct.two;

    let func_name = format_ident!("get_{}_count", postfix);

    let gen = quote! {
        pub async fn #func_name(
        conn: Arc<DatabaseConnection>,
    ) -> GringottsResult<u64> {
        Ok(#entity::find().count(conn.as_ref()).await?)
    }
    };

    gen.into()
}

#[proc_macro]
pub fn impl_stream_all_items(input: TokenStream) -> TokenStream {
    let input_struct = syn::parse_macro_input!(input as TypeTriplet);

    let entity = &input_struct.one;

    let model = &input_struct.two;

    let postfix = &input_struct.three;

    let func_name = format_ident!("stream_all_{}", postfix);

    let gen = quote! {
        pub async fn #func_name(
        &self,
        conn: Arc<DatabaseConnection>,
        ) -> impl Stream<Item = GringottsResult<#model>> {
            stream! {
                let conn_owned = conn.clone();
                let mut db_stream = #entity::find().stream(conn_owned.as_ref()).await?;


                while let Some(item) = db_stream.try_next().await? {
                    yield Ok(item)
                }
            }
        }
    };

    gen.into()
}

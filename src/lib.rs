#![allow(clippy::multiple_crate_versions)]
pub use shared::{
    entity::sea_orm_active_enums::*,
    queries::Queries,
    ApplicabilityMatrixModel,
    Enabler,
    GringottsError,
    GringottsResult,
    Service,
};

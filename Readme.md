# Gringotts

This projects is the base ground for all `Facts` for **AI-Dojo**.

`Facts` are for example:
  - a collection and usage information about software (via CPE)
  - `Enablers`: a collection of vulnerabilities & weaknesses and information (via CVE, CWE)
  - relations between software, enablers and attack techniques
  - deployment data for docker stuf
  - **...**

## Structure

 *  `web_api` contains an actix-web REST API for querying the database.
 *  `migrations` hold the database migration "scripts" and wrapper.
 *  `populate` contains the logic to fill the databases.
 *  `shared` hold entities used throughout all the tooling, and models for streaming query results.
 *  `proc_macros` contains metaprogramming primitives used for code amount minimization.
 *  `src` exports the artifacts into a library. These artifacts can be useful on the client side.


## Deployment

**Requirements**: docker, rust & cargo (see https://www.rust-lang.org/tools/install; we use nightly builds, set it with `rustup default nightly`). 

  - Clone the repo and `cd` into it.
  - Fill in the `.env.template` file with actual secrets, and rename it to `.env`.
  - `docker compose up -d` to spawn the Postgres database.
  - `cargo build --release --workspace` to build the maintenance & api binaries.
  - `./target/release/migration<.exe>` to run database migrations. (Only do this on first startup.)
  - `./target/release/populate<.exe>` to populate the database. See the executables' options. The flag `--real` starts population by crawling online sources and databases (**not yet implemented**). The `--mock <scenario>` option populates the database artificially by the selected scenario.
  - `./target/release/web_api --interface <all/localhost> &` to start the REST API. The interface should be chosen based on where users of the database will reside. 

## For developers

### Creating new tables

New tables can be created by new migration scripts. Keep to the naming convention of the files as seen in `migrations/src`.
Code your migration: see the [sea-orm migration documentation](https://www.sea-ql.org/SeaORM/docs/next/migration/writing-migration/), then enroll your migration in `migration/src/lib.rs`.
If you need entities(models) for Rust, you can do that via the [sea-orm-cli](https://www.sea-ql.org/SeaORM/docs/next/generate-entity/sea-orm-cli/). *Here, be careful not to overwrite the existing entity files, and enums should be appended to the already existing `sea_orm_active_enums.rs` file.*

#![allow(clippy::multiple_crate_versions)]
use clap::{command, Parser};
use sea_orm::{ConnectOptions, ConnectionTrait, Database};

mod mock;
mod realistic;

use mock::MockScenarios;

#[derive(Parser, Debug)]
#[command(author, version, about, long_about=None, arg_required_else_help = true)]
struct MainArgs {
    #[arg(long, conflicts_with = "mock")]
    real: bool,

    #[arg(long, conflicts_with = "real")]
    mock: Option<MockScenarios>,
}

#[tokio::main]
async fn main() -> std::io::Result<()> {
    let args = MainArgs::parse();
    dotenvy::dotenv().expect("Could not read `.env` file.");
    std::env::set_var("RUST_LOG", "info");
    std::env::set_var("RUST_BACKTRACE", "1");
    env_logger::init();
    let db_url = std::env::var("DATABASE_URL").expect("Environment variable `DATABASE_URL` must be set.");

    let connection_pool = Database::connect(ConnectOptions::new(&db_url))
        .await
        .expect("Could not connect to database.");

    match connection_pool.get_database_backend() {
        sea_orm::DatabaseBackend::MySql => panic!("MySQL connection detected, expecting Postgres"),
        sea_orm::DatabaseBackend::Postgres => {}
        sea_orm::DatabaseBackend::Sqlite => {
            panic!("SQLite connection detected, expecting Postgres")
        }
    };
    if args.real {
        todo!()
    } else if let Some(scenario) = args.mock {
        scenario.call(&connection_pool).await.unwrap();
    }
    Ok(())
}

use sea_orm::{DatabaseConnection, DbErr};
use serde_json::json;
use shared::{
    entity::{
        prelude::{EnablerModel, FunctionalDataModel, ServiceImpactModel, ServiceModel, ServiceTagModel},
        sea_orm_active_enums::{DbPrivilegeLevel, EnablerType, Impact, Tag},
    },
    mutations::Mutations,
};

pub async fn mock(db: &DatabaseConnection) -> Result<(), DbErr> {
    let insider_job = Mutations::save_service(
        ServiceModel {
            name:    String::from("guy"),
            vendor:  String::from("god himself"),
            version: String::from("1.0.0"),
            id:      0,
        },
        db,
    );
    let smb_job = Mutations::save_service(
        ServiceModel {
            name:    String::from("smb"),
            vendor:  String::from("smb"),
            version: String::from("1.0.0"),
            id:      0,
        },
        db,
    );
    let powershell_job = Mutations::save_service(
        ServiceModel {
            name:    String::from("powershell"),
            vendor:  String::from("Microsoft"),
            version: String::from("1.0.0"),
            id:      0,
        },
        db,
    );
    let spoolsv_job = Mutations::save_service(
        ServiceModel {
            name:    String::from("printspooler"),
            vendor:  String::from("Microsoft"),
            version: String::from("1.0.0"),
            id:      0,
        },
        db,
    );
    let tasksched_job = Mutations::save_service(
        ServiceModel {
            name:    String::from("TaskScheduler"),
            vendor:  String::from("Microsoft"),
            version: String::from("1.0.0"),
            id:      0,
        },
        db,
    );
    let driver_manager_job = Mutations::save_service(
        ServiceModel {
            name:    String::from("Msdrivermanager"),
            vendor:  String::from("Microsoft"),
            version: String::from("1.0.0"),
            id:      0,
        },
        db,
    );
    let sevenstep_job = Mutations::save_service(
        ServiceModel {
            name:    String::from("7step"),
            vendor:  String::from("siemens"),
            version: String::from("1.0.0"),
            id:      0,
        },
        db,
    );

    let (insider, smb, powershell, spoolsv, tasksched, driver_manager, sevenstep) = tokio::join!(
        insider_job,
        smb_job,
        powershell_job,
        spoolsv_job,
        tasksched_job,
        driver_manager_job,
        sevenstep_job
    );

    let func_data = vec![
        FunctionalDataModel {
            id:                0,
            auto_elevation:    Some(false),
            executable_access: Some(DbPrivilegeLevel::User),
            target_software:   Some(String::from("windows")),
            service:           powershell.as_ref().unwrap().id.clone().unwrap(),
            ports:             None,
        },
        FunctionalDataModel {
            id:                0,
            auto_elevation:    Some(true),
            executable_access: Some(DbPrivilegeLevel::Administrator),
            target_software:   Some(String::from("*")),
            service:           smb.as_ref().unwrap().id.clone().unwrap(),
            ports:             Some(vec![139, 445]),
        },
        FunctionalDataModel {
            id:                0,
            auto_elevation:    Some(true),
            executable_access: Some(DbPrivilegeLevel::Administrator),
            target_software:   Some(String::from("windows")),
            service:           spoolsv.as_ref().unwrap().id.clone().unwrap(),
            ports:             Some(vec![445]),
        },
        FunctionalDataModel {
            id:                0,
            auto_elevation:    Some(true),
            executable_access: Some(DbPrivilegeLevel::Administrator),
            target_software:   Some(String::from("windows")),
            service:           tasksched.as_ref().unwrap().id.clone().unwrap(),
            ports:             None,
        },
        FunctionalDataModel {
            id:                0,
            auto_elevation:    Some(true),
            executable_access: Some(DbPrivilegeLevel::Administrator),
            target_software:   Some(String::from("windows")),
            service:           driver_manager.as_ref().unwrap().id.clone().unwrap(),
            ports:             None,
        },
        FunctionalDataModel {
            id:                0,
            auto_elevation:    Some(false),
            executable_access: Some(DbPrivilegeLevel::None),
            target_software:   Some(String::from("*")),
            service:           insider.as_ref().unwrap().id.clone().unwrap(),
            ports:             None,
        },
        FunctionalDataModel {
            id:                0,
            auto_elevation:    Some(false),
            executable_access: Some(DbPrivilegeLevel::Administrator),
            target_software:   Some(String::from("windows")),
            service:           sevenstep.as_ref().unwrap().id.clone().unwrap(),
            ports:             None,
        },
    ];

    let tags = vec![
        ServiceTagModel {
            service_id: powershell.as_ref().unwrap().id.clone().unwrap(),
            tag:        Tag::Terminal,
        },
        ServiceTagModel {
            service_id: powershell.as_ref().unwrap().id.clone().unwrap(),
            tag:        Tag::AdministrativeTool,
        },
        ServiceTagModel {
            service_id: smb.as_ref().unwrap().id.clone().unwrap(),
            tag:        Tag::AdministrativeTool,
        },
        ServiceTagModel {
            service_id: smb.as_ref().unwrap().id.clone().unwrap(),
            tag:        Tag::FileSharing,
        },
        ServiceTagModel {
            service_id: spoolsv.as_ref().unwrap().id.clone().unwrap(),
            tag:        Tag::SystemComponent,
        },
        ServiceTagModel {
            service_id: tasksched.as_ref().unwrap().id.clone().unwrap(),
            tag:        Tag::SystemComponent,
        },
        ServiceTagModel {
            service_id: driver_manager.as_ref().unwrap().id.clone().unwrap(),
            tag:        Tag::SystemComponent,
        },
        ServiceTagModel {
            service_id: insider.as_ref().unwrap().id.clone().unwrap(),
            tag:        Tag::App,
        },
        ServiceTagModel {
            service_id: insider.as_ref().unwrap().id.clone().unwrap(),
            tag:        Tag::App,
        },
    ];

    let impacts = vec![
        ServiceImpactModel {
            service_id: powershell.as_ref().unwrap().id.clone().unwrap(),
            impact:     Impact::ExecuteCode,
        },
        ServiceImpactModel {
            service_id: powershell.as_ref().unwrap().id.clone().unwrap(),
            impact:     Impact::ReadFile,
        },
        ServiceImpactModel {
            service_id: powershell.as_ref().unwrap().id.clone().unwrap(),
            impact:     Impact::WriteFile,
        },
        ServiceImpactModel {
            service_id: smb.as_ref().unwrap().id.clone().unwrap(),
            impact:     Impact::ReadFile,
        },
        ServiceImpactModel {
            service_id: smb.as_ref().unwrap().id.clone().unwrap(),
            impact:     Impact::WriteFile,
        },
        ServiceImpactModel {
            service_id: spoolsv.as_ref().unwrap().id.clone().unwrap(),
            impact:     Impact::ExecuteCode,
        },
        ServiceImpactModel {
            service_id: driver_manager.as_ref().unwrap().id.clone().unwrap(),
            impact:     Impact::ExecuteCode,
        },
        ServiceImpactModel {
            service_id: tasksched.as_ref().unwrap().id.clone().unwrap(),
            impact:     Impact::ExecuteCode,
        },
    ];

    let func_data_job = Mutations::insert_multiple_functional_data(func_data, db);
    let tags_job = Mutations::insert_multiple_tags(tags, db);
    let impact_job = Mutations::insert_multiple_impacts(impacts, db);

    let (func_res, tag_res, impact_res) = tokio::join!(func_data_job, tags_job, impact_job);
    func_res?;
    tag_res?;
    impact_res?;

    Mutations::insert_enabler(
        EnablerModel {
            id:       0,
            r#type:   Option::from(EnablerType::Cve),
            details:  Option::from(json!("dummy")),
            main_cpe: String::new(),
            config:   None,
        },
        db,
    )
    .await?;

    Ok(())
}

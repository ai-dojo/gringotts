use sea_orm::{DatabaseConnection, DbErr};
use serde_json::json;
use shared::{
    entity::{
        prelude::{EnablerModel, FunctionalDataModel, ServiceImpactModel, ServiceModel, ServiceTagModel},
        sea_orm_active_enums::{DbPrivilegeLevel, EnablerType, Impact, Tag},
    },
    mutations::Mutations,
};

pub async fn mock(db: &DatabaseConnection) -> Result<(), DbErr> {
    let ssh_job = Mutations::save_service(
        ServiceModel {
            name:    String::from("ssh"),
            vendor:  String::from("OpenSSH"),
            version: String::from("1.0.0"),
            id:      0,
        },
        db,
    );
    let rdp_job = Mutations::save_service(
        ServiceModel {
            name:    String::from("rdp"),
            vendor:  String::from("Microsoft"),
            version: String::from("1.0.0"),
            id:      0,
        },
        db,
    );
    let powershell_job = Mutations::save_service(
        ServiceModel {
            name:    String::from("powershell"),
            vendor:  String::from("Microsoft"),
            version: String::from("1.0.0"),
            id:      0,
        },
        db,
    );
    let vmware_job = Mutations::save_service(
        ServiceModel {
            name:    String::from("VMWareHorizon"),
            vendor:  String::from("VmWare"),
            version: String::from("1.0.0"),
            id:      0,
        },
        db,
    );
    let samregistry_job = Mutations::save_service(
        ServiceModel {
            name:    String::from("samregistry"),
            vendor:  String::from("Microsoft"),
            version: String::from("1.0.0"),
            id:      0,
        },
        db,
    );

    let (ssh, rdp, powershell, samregistry, vmware) =
        tokio::join!(ssh_job, rdp_job, powershell_job, samregistry_job, vmware_job);

    let func_data = vec![
        FunctionalDataModel {
            id:                0,
            auto_elevation:    Some(true),
            executable_access: Some(DbPrivilegeLevel::Administrator),
            target_software:   Some(String::from("*")),
            service:           ssh.as_ref().unwrap().id.clone().unwrap(),
            ports:             Some(vec![22]),
        },
        FunctionalDataModel {
            id:                0,
            auto_elevation:    Some(true),
            executable_access: Some(DbPrivilegeLevel::Administrator),
            target_software:   Some(String::from("windows")),
            service:           rdp.as_ref().unwrap().id.clone().unwrap(),
            ports:             Some(vec![3389]),
        },
        FunctionalDataModel {
            id:                0,
            auto_elevation:    Some(false),
            executable_access: Some(DbPrivilegeLevel::User),
            target_software:   Some(String::from("windows")),
            service:           powershell.as_ref().unwrap().id.clone().unwrap(),
            ports:             None,
        },
        FunctionalDataModel {
            id:                0,
            auto_elevation:    Some(false),
            executable_access: Some(DbPrivilegeLevel::Administrator),
            target_software:   Some(String::from("windows")),
            service:           samregistry.as_ref().unwrap().id.clone().unwrap(),
            ports:             None,
        },
        FunctionalDataModel {
            id:                0,
            auto_elevation:    Some(true),
            executable_access: Some(DbPrivilegeLevel::Administrator),
            target_software:   Some(String::from("windows")),
            service:           vmware.as_ref().unwrap().id.clone().unwrap(),
            ports:             Some(vec![6578]),
        },
    ];

    let tags = vec![
        ServiceTagModel {
            service_id: ssh.as_ref().unwrap().id.clone().unwrap(),
            tag:        Tag::RemoteAccess,
        },
        ServiceTagModel {
            service_id: rdp.as_ref().unwrap().id.clone().unwrap(),
            tag:        Tag::RemoteAccess,
        },
        ServiceTagModel {
            service_id: powershell.as_ref().unwrap().id.clone().unwrap(),
            tag:        Tag::Terminal,
        },
        ServiceTagModel {
            service_id: powershell.as_ref().unwrap().id.clone().unwrap(),
            tag:        Tag::AdministrativeTool,
        },
        ServiceTagModel {
            service_id: samregistry.as_ref().unwrap().id.clone().unwrap(),
            tag:        Tag::SystemComponent,
        },
        ServiceTagModel {
            service_id: vmware.as_ref().unwrap().id.clone().unwrap(),
            tag:        Tag::VirtualizationPlatform,
        },
    ];

    let impacts = vec![
        ServiceImpactModel {
            service_id: ssh.as_ref().unwrap().id.clone().unwrap(),
            impact:     Impact::ExecuteCode,
        },
        ServiceImpactModel {
            service_id: ssh.as_ref().unwrap().id.clone().unwrap(),
            impact:     Impact::ReadFile,
        },
        ServiceImpactModel {
            service_id: ssh.as_ref().unwrap().id.clone().unwrap(),
            impact:     Impact::WriteFile,
        },
        ServiceImpactModel {
            service_id: powershell.as_ref().unwrap().id.clone().unwrap(),
            impact:     Impact::ExecuteCode,
        },
        ServiceImpactModel {
            service_id: powershell.as_ref().unwrap().id.clone().unwrap(),
            impact:     Impact::ReadFile,
        },
        ServiceImpactModel {
            service_id: powershell.as_ref().unwrap().id.clone().unwrap(),
            impact:     Impact::WriteFile,
        },
        ServiceImpactModel {
            service_id: vmware.as_ref().unwrap().id.clone().unwrap(),
            impact:     Impact::ExecuteCode,
        },
        ServiceImpactModel {
            service_id: vmware.as_ref().unwrap().id.clone().unwrap(),
            impact:     Impact::ReadFile,
        },
        ServiceImpactModel {
            service_id: vmware.as_ref().unwrap().id.clone().unwrap(),
            impact:     Impact::WriteFile,
        },
    ];

    let func_data_job = Mutations::insert_multiple_functional_data(func_data, db);
    let tags_job = Mutations::insert_multiple_tags(tags, db);
    let impact_job = Mutations::insert_multiple_impacts(impacts, db);

    let (func_res, tag_res, impact_res) = tokio::join!(func_data_job, tags_job, impact_job);
    func_res?;
    tag_res?;
    impact_res?;

    Mutations::insert_enabler(
        EnablerModel {
            id:       0,
            r#type:   Option::from(EnablerType::Cve),
            details:  Option::from(json!("dummy")),
            main_cpe: String::new(),
            config:   None,
        },
        db,
    )
    .await?;

    Ok(())
}

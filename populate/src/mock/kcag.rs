use sea_orm::{DatabaseConnection, DbErr};
use serde_json::json;
use shared::{
    entity::{
        prelude::{EnablerModel, FunctionalDataModel, ServiceImpactModel, ServiceModel, ServiceTagModel},
        sea_orm_active_enums::{DbPrivilegeLevel, EnablerType, Impact, Tag},
    },
    mutations::Mutations,
};

pub async fn mock(db: &DatabaseConnection) -> Result<(), DbErr> {
    let ssh_job = Mutations::save_service(
        ServiceModel {
            name:    String::from("ssh"),
            vendor:  String::from("OpenSSH"),
            version: String::from("1.0.0"),
            id:      0,
        },
        db,
    );
    let rdp_job = Mutations::save_service(
        ServiceModel {
            name:    String::from("rdp"),
            vendor:  String::from("Microsoft"),
            version: String::from("1.0.0"),
            id:      0,
        },
        db,
    );
    let vnc_job = Mutations::save_service(
        ServiceModel {
            name:    String::from("openVNC"),
            vendor:  String::from("openvnc"),
            version: String::from("1.0.0"),
            id:      0,
        },
        db,
    );
    let nginx_job = Mutations::save_service(
        ServiceModel {
            name:    String::from("nginx"),
            vendor:  String::from("nginx"),
            version: String::from("1.0.0"),
            id:      0,
        },
        db,
    );
    let apache_job = Mutations::save_service(
        ServiceModel {
            name:    String::from("apache-web"),
            vendor:  String::from("apache"),
            version: String::from("1.0.0"),
            id:      0,
        },
        db,
    );
    let outlook_job = Mutations::save_service(
        ServiceModel {
            name:    String::from("outlook"),
            vendor:  String::from("Microsoft"),
            version: String::from("1.0.0"),
            id:      0,
        },
        db,
    );
    let thunderbird_job = Mutations::save_service(
        ServiceModel {
            name:    String::from("thunderbird"),
            vendor:  String::from("thunderbird"),
            version: String::from("1.0.0"),
            id:      0,
        },
        db,
    );
    let msoffice_job = Mutations::save_service(
        ServiceModel {
            name:    String::from("MSoffice"),
            vendor:  String::from("Microsoft"),
            version: String::from("1.0.0"),
            id:      0,
        },
        db,
    );
    let firefox_job = Mutations::save_service(
        ServiceModel {
            name:    String::from("Firefox"),
            vendor:  String::from("Mozilla"),
            version: String::from("1.0.0"),
            id:      0,
        },
        db,
    );
    let edge_job = Mutations::save_service(
        ServiceModel {
            name:    String::from("Edge"),
            vendor:  String::from("Microsoft"),
            version: String::from("1.0.0"),
            id:      0,
        },
        db,
    );
    let wireguard_job = Mutations::save_service(
        ServiceModel {
            name:    String::from("wireguard"),
            vendor:  String::from("wireguard"),
            version: String::from("1.0.0"),
            id:      0,
        },
        db,
    );
    let skype_job = Mutations::save_service(
        ServiceModel {
            name:    String::from("skype"),
            vendor:  String::from("Microsoft"),
            version: String::from("1.0.0"),
            id:      0,
        },
        db,
    );
    let redis_job = Mutations::save_service(
        ServiceModel {
            name:    String::from("redisStack"),
            vendor:  String::from("redhat"),
            version: String::from("1.0.0"),
            id:      0,
        },
        db,
    );
    let mysql_job = Mutations::save_service(
        ServiceModel {
            name:    String::from("mysql"),
            vendor:  String::from("mysql"),
            version: String::from("1.0.0"),
            id:      0,
        },
        db,
    );
    let smb_job = Mutations::save_service(
        ServiceModel {
            name:    String::from("smb"),
            vendor:  String::from("smb"),
            version: String::from("1.0.0"),
            id:      0,
        },
        db,
    );
    let ftp_job = Mutations::save_service(
        ServiceModel {
            name:    String::from("freeFTP"),
            vendor:  String::from("freeFTP"),
            version: String::from("1.0.0"),
            id:      0,
        },
        db,
    );
    let docker_job = Mutations::save_service(
        ServiceModel {
            name:    String::from("docker-daemon"),
            vendor:  String::from("docker"),
            version: String::from("1.0.0"),
            id:      0,
        },
        db,
    );
    let powershell_job = Mutations::save_service(
        ServiceModel {
            name:    String::from("powershell"),
            vendor:  String::from("Microsoft"),
            version: String::from("1.0.0"),
            id:      0,
        },
        db,
    );
    let bash_job = Mutations::save_service(
        ServiceModel {
            name:    String::from("bash"),
            vendor:  String::from("Linus"),
            version: String::from("1.0.0"),
            id:      0,
        },
        db,
    );
    let spoolsv_job = Mutations::save_service(
        ServiceModel {
            name:    String::from("printspooler"),
            vendor:  String::from("Microsoft"),
            version: String::from("1.0.0"),
            id:      0,
        },
        db,
    );
    let sudo_job = Mutations::save_service(
        ServiceModel {
            name:    String::from("sudo"),
            vendor:  String::from("Linus"),
            version: String::from("1.0.0"),
            id:      0,
        },
        db,
    );
    let custom_job = Mutations::save_service(
        ServiceModel {
            name:    String::from("customapp"),
            vendor:  String::from("we"),
            version: String::from("1.0.0"),
            id:      0,
        },
        db,
    );
    let telnet_job = Mutations::save_service(
        ServiceModel {
            name:    String::from("telnet"),
            vendor:  String::from("CERN"),
            version: String::from("1.0.0"),
            id:      0,
        },
        db,
    );
    let winrm_job = Mutations::save_service(
        ServiceModel {
            name:    String::from("winRM"),
            vendor:  String::from("Microsoft"),
            version: String::from("1.0.0"),
            id:      0,
        },
        db,
    );
    let teamviewer_job = Mutations::save_service(
        ServiceModel {
            name:    String::from("TeamViewer"),
            vendor:  String::from("teamwieverteam"),
            version: String::from("1.0.0"),
            id:      0,
        },
        db,
    );
    let winlogon_job = Mutations::save_service(
        ServiceModel {
            name:    String::from("winlogon"),
            vendor:  String::from("Microsoft"),
            version: String::from("1.0.0"),
            id:      0,
        },
        db,
    );
    let lightdm_job = Mutations::save_service(
        ServiceModel {
            name:    String::from("lightdm"),
            vendor:  String::from("Linus"),
            version: String::from("1.0.0"),
            id:      0,
        },
        db,
    );

    let (
        ssh,
        rdp,
        openvnc,
        nginx,
        apache,
        outlook,
        thunderbird,
        msoffice,
        firefox,
        edge,
        wireguard,
        skype,
        redis,
        mysql,
        smb,
        ftp,
        docker,
        powershell,
        bash,
        spoolsv,
        sudo,
        custom,
        telnet,
        winrm,
        teamviewer,
        winlogon,
        lightdm,
    ) = tokio::join!(
        ssh_job,
        rdp_job,
        vnc_job,
        nginx_job,
        apache_job,
        outlook_job,
        thunderbird_job,
        msoffice_job,
        firefox_job,
        edge_job,
        wireguard_job,
        skype_job,
        redis_job,
        mysql_job,
        smb_job,
        ftp_job,
        docker_job,
        powershell_job,
        bash_job,
        spoolsv_job,
        sudo_job,
        custom_job,
        telnet_job,
        winrm_job,
        teamviewer_job,
        winlogon_job,
        lightdm_job
    );

    let func_data = vec![
        FunctionalDataModel {
            id:                0,
            auto_elevation:    Some(true),
            executable_access: Some(DbPrivilegeLevel::Administrator),
            target_software:   Some(String::from("*")),
            service:           ssh.as_ref().unwrap().id.clone().unwrap(),
            ports:             Some(vec![22]),
        },
        FunctionalDataModel {
            id:                0,
            auto_elevation:    Some(true),
            executable_access: Some(DbPrivilegeLevel::Administrator),
            target_software:   Some(String::from("windows")),
            service:           rdp.as_ref().unwrap().id.clone().unwrap(),
            ports:             Some(vec![3389]),
        },
        FunctionalDataModel {
            id:                0,
            auto_elevation:    Some(true),
            executable_access: Some(DbPrivilegeLevel::Administrator),
            target_software:   Some(String::from("*")),
            service:           openvnc.as_ref().unwrap().id.clone().unwrap(),
            ports:             Some(vec![1194]),
        },
        FunctionalDataModel {
            id:                0,
            auto_elevation:    Some(false),
            executable_access: Some(DbPrivilegeLevel::Service),
            target_software:   Some(String::from("*")),
            service:           nginx.as_ref().unwrap().id.clone().unwrap(),
            ports:             Some(vec![80, 443]),
        },
        FunctionalDataModel {
            id:                0,
            auto_elevation:    Some(false),
            executable_access: Some(DbPrivilegeLevel::Service),
            target_software:   Some(String::from("*")),
            service:           apache.as_ref().unwrap().id.clone().unwrap(),
            ports:             Some(vec![80, 443]),
        },
        FunctionalDataModel {
            id:                0,
            auto_elevation:    Some(false),
            executable_access: Some(DbPrivilegeLevel::User),
            target_software:   Some(String::from("windows")),
            service:           outlook.as_ref().unwrap().id.clone().unwrap(),
            ports:             None,
        },
        FunctionalDataModel {
            id:                0,
            auto_elevation:    Some(false),
            executable_access: Some(DbPrivilegeLevel::User),
            target_software:   Some(String::from("*")),
            service:           thunderbird.as_ref().unwrap().id.clone().unwrap(),
            ports:             None,
        },
        FunctionalDataModel {
            id:                0,
            auto_elevation:    Some(false),
            executable_access: Some(DbPrivilegeLevel::User),
            target_software:   Some(String::from("windows")),
            service:           msoffice.as_ref().unwrap().id.clone().unwrap(),
            ports:             None,
        },
        FunctionalDataModel {
            id:                0,
            auto_elevation:    Some(false),
            executable_access: Some(DbPrivilegeLevel::User),
            target_software:   Some(String::from("*")),
            service:           firefox.as_ref().unwrap().id.clone().unwrap(),
            ports:             None,
        },
        FunctionalDataModel {
            id:                0,
            auto_elevation:    Some(false),
            executable_access: Some(DbPrivilegeLevel::User),
            target_software:   Some(String::from("windows")),
            service:           edge.as_ref().unwrap().id.clone().unwrap(),
            ports:             None,
        },
        FunctionalDataModel {
            id:                0,
            auto_elevation:    Some(false),
            executable_access: Some(DbPrivilegeLevel::User),
            target_software:   Some(String::from("*")),
            service:           wireguard.as_ref().unwrap().id.clone().unwrap(),
            ports:             Some(vec![51820]),
        },
        FunctionalDataModel {
            id:                0,
            auto_elevation:    Some(false),
            executable_access: Some(DbPrivilegeLevel::User),
            target_software:   Some(String::from("windows")),
            service:           skype.as_ref().unwrap().id.clone().unwrap(),
            ports:             None,
        },
        FunctionalDataModel {
            id:                0,
            auto_elevation:    Some(false),
            executable_access: Some(DbPrivilegeLevel::Service),
            target_software:   Some(String::from("*")),
            service:           redis.as_ref().unwrap().id.clone().unwrap(),
            ports:             Some(vec![6379]),
        },
        FunctionalDataModel {
            id:                0,
            auto_elevation:    Some(false),
            executable_access: Some(DbPrivilegeLevel::Service),
            target_software:   Some(String::from("*")),
            service:           mysql.as_ref().unwrap().id.clone().unwrap(),
            ports:             Some(vec![3306]),
        },
        FunctionalDataModel {
            id:                0,
            auto_elevation:    Some(true),
            executable_access: Some(DbPrivilegeLevel::Administrator),
            target_software:   Some(String::from("*")),
            service:           smb.as_ref().unwrap().id.clone().unwrap(),
            ports:             Some(vec![139, 445]),
        },
        FunctionalDataModel {
            id:                0,
            auto_elevation:    Some(true),
            executable_access: Some(DbPrivilegeLevel::Administrator),
            target_software:   Some(String::from("*")),
            service:           ftp.as_ref().unwrap().id.clone().unwrap(),
            ports:             Some(vec![20, 21]),
        },
        FunctionalDataModel {
            id:                0,
            auto_elevation:    Some(false),
            executable_access: Some(DbPrivilegeLevel::User),
            target_software:   Some(String::from("*")),
            service:           docker.as_ref().unwrap().id.clone().unwrap(),
            ports:             None,
        },
        FunctionalDataModel {
            id:                0,
            auto_elevation:    Some(false),
            executable_access: Some(DbPrivilegeLevel::User),
            target_software:   Some(String::from("windows")),
            service:           powershell.as_ref().unwrap().id.clone().unwrap(),
            ports:             None,
        },
        FunctionalDataModel {
            id:                0,
            auto_elevation:    Some(false),
            executable_access: Some(DbPrivilegeLevel::User),
            target_software:   Some(String::from("*nix")),
            service:           bash.as_ref().unwrap().id.clone().unwrap(),
            ports:             None,
        },
        FunctionalDataModel {
            id:                0,
            auto_elevation:    Some(true),
            executable_access: Some(DbPrivilegeLevel::Administrator),
            target_software:   Some(String::from("windows")),
            service:           spoolsv.as_ref().unwrap().id.clone().unwrap(),
            ports:             Some(vec![445]),
        },
        FunctionalDataModel {
            id:                0,
            auto_elevation:    Some(true),
            executable_access: Some(DbPrivilegeLevel::Administrator),
            target_software:   Some(String::from("*nix")),
            service:           sudo.as_ref().unwrap().id.clone().unwrap(),
            ports:             None,
        },
        FunctionalDataModel {
            id:                0,
            auto_elevation:    Some(true),
            executable_access: Some(DbPrivilegeLevel::User),
            target_software:   Some(String::from("*")),
            service:           custom.as_ref().unwrap().id.clone().unwrap(),
            ports:             None,
        },
        FunctionalDataModel {
            id:                0,
            auto_elevation:    Some(true),
            executable_access: Some(DbPrivilegeLevel::Administrator),
            target_software:   Some(String::from("*nix")),
            service:           telnet.as_ref().unwrap().id.clone().unwrap(),
            ports:             Some(vec![23]),
        },
        FunctionalDataModel {
            id:                0,
            auto_elevation:    Some(true),
            executable_access: Some(DbPrivilegeLevel::Administrator),
            target_software:   Some(String::from("windows")),
            service:           winrm.as_ref().unwrap().id.clone().unwrap(),
            ports:             Some(vec![5985, 5986]),
        },
        FunctionalDataModel {
            id:                0,
            auto_elevation:    Some(true),
            executable_access: Some(DbPrivilegeLevel::Administrator),
            target_software:   Some(String::from("windows")),
            service:           teamviewer.as_ref().unwrap().id.clone().unwrap(),
            ports:             Some(vec![5938, 443]),
        },
        FunctionalDataModel {
            id:                0,
            auto_elevation:    Some(true),
            executable_access: Some(DbPrivilegeLevel::LocalSystem),
            target_software:   Some(String::from("windows")),
            service:           winlogon.as_ref().unwrap().id.clone().unwrap(),
            ports:             None,
        },
        FunctionalDataModel {
            id:                0,
            auto_elevation:    Some(true),
            executable_access: Some(DbPrivilegeLevel::LocalSystem),
            target_software:   Some(String::from("*nix")),
            service:           lightdm.as_ref().unwrap().id.clone().unwrap(),
            ports:             None,
        },
    ];

    let tags = vec![
        ServiceTagModel {
            service_id: ssh.as_ref().unwrap().id.clone().unwrap(),
            tag:        Tag::RemoteAccess,
        },
        ServiceTagModel {
            service_id: rdp.as_ref().unwrap().id.clone().unwrap(),
            tag:        Tag::RemoteAccess,
        },
        ServiceTagModel {
            service_id: openvnc.as_ref().unwrap().id.clone().unwrap(),
            tag:        Tag::RemoteAccess,
        },
        ServiceTagModel {
            service_id: nginx.as_ref().unwrap().id.clone().unwrap(),
            tag:        Tag::WebHosting,
        },
        ServiceTagModel {
            service_id: apache.as_ref().unwrap().id.clone().unwrap(),
            tag:        Tag::WebHosting,
        },
        ServiceTagModel {
            service_id: outlook.as_ref().unwrap().id.clone().unwrap(),
            tag:        Tag::OfficeApplication,
        },
        ServiceTagModel {
            service_id: outlook.as_ref().unwrap().id.clone().unwrap(),
            tag:        Tag::EmailClient,
        },
        ServiceTagModel {
            service_id: thunderbird.as_ref().unwrap().id.clone().unwrap(),
            tag:        Tag::OfficeApplication,
        },
        ServiceTagModel {
            service_id: thunderbird.as_ref().unwrap().id.clone().unwrap(),
            tag:        Tag::EmailClient,
        },
        ServiceTagModel {
            service_id: msoffice.as_ref().unwrap().id.clone().unwrap(),
            tag:        Tag::OfficeApplication,
        },
        ServiceTagModel {
            service_id: firefox.as_ref().unwrap().id.clone().unwrap(),
            tag:        Tag::WebBrowser,
        },
        ServiceTagModel {
            service_id: edge.as_ref().unwrap().id.clone().unwrap(),
            tag:        Tag::WebBrowser,
        },
        ServiceTagModel {
            service_id: wireguard.as_ref().unwrap().id.clone().unwrap(),
            tag:        Tag::RemoteAccess,
        },
        ServiceTagModel {
            service_id: skype.as_ref().unwrap().id.clone().unwrap(),
            tag:        Tag::Teleconference,
        },
        ServiceTagModel {
            service_id: redis.as_ref().unwrap().id.clone().unwrap(),
            tag:        Tag::Database,
        },
        ServiceTagModel {
            service_id: mysql.as_ref().unwrap().id.clone().unwrap(),
            tag:        Tag::Database,
        },
        ServiceTagModel {
            service_id: smb.as_ref().unwrap().id.clone().unwrap(),
            tag:        Tag::AdministrativeTool,
        },
        ServiceTagModel {
            service_id: smb.as_ref().unwrap().id.clone().unwrap(),
            tag:        Tag::FileSharing,
        },
        ServiceTagModel {
            service_id: ftp.as_ref().unwrap().id.clone().unwrap(),
            tag:        Tag::FileSharing,
        },
        ServiceTagModel {
            service_id: docker.as_ref().unwrap().id.clone().unwrap(),
            tag:        Tag::VirtualizationPlatform,
        },
        ServiceTagModel {
            service_id: powershell.as_ref().unwrap().id.clone().unwrap(),
            tag:        Tag::Terminal,
        },
        ServiceTagModel {
            service_id: powershell.as_ref().unwrap().id.clone().unwrap(),
            tag:        Tag::AdministrativeTool,
        },
        ServiceTagModel {
            service_id: bash.as_ref().unwrap().id.clone().unwrap(),
            tag:        Tag::Terminal,
        },
        ServiceTagModel {
            service_id: bash.as_ref().unwrap().id.clone().unwrap(),
            tag:        Tag::AdministrativeTool,
        },
        ServiceTagModel {
            service_id: spoolsv.as_ref().unwrap().id.clone().unwrap(),
            tag:        Tag::SystemComponent,
        },
        ServiceTagModel {
            service_id: sudo.as_ref().unwrap().id.clone().unwrap(),
            tag:        Tag::SystemComponent,
        },
        ServiceTagModel {
            service_id: telnet.as_ref().unwrap().id.clone().unwrap(),
            tag:        Tag::RemoteAccess,
        },
        ServiceTagModel {
            service_id: winrm.as_ref().unwrap().id.clone().unwrap(),
            tag:        Tag::AdministrativeTool,
        },
        ServiceTagModel {
            service_id: teamviewer.as_ref().unwrap().id.clone().unwrap(),
            tag:        Tag::RemoteAccess,
        },
        ServiceTagModel {
            service_id: lightdm.as_ref().unwrap().id.clone().unwrap(),
            tag:        Tag::SystemComponent,
        },
        ServiceTagModel {
            service_id: winlogon.as_ref().unwrap().id.clone().unwrap(),
            tag:        Tag::SystemComponent,
        },
        ServiceTagModel {
            service_id: custom.as_ref().unwrap().id.clone().unwrap(),
            tag:        Tag::App,
        },
    ];

    let impacts = vec![
        ServiceImpactModel {
            service_id: ssh.as_ref().unwrap().id.clone().unwrap(),
            impact:     Impact::ExecuteCode,
        },
        ServiceImpactModel {
            service_id: ssh.as_ref().unwrap().id.clone().unwrap(),
            impact:     Impact::ReadFile,
        },
        ServiceImpactModel {
            service_id: ssh.as_ref().unwrap().id.clone().unwrap(),
            impact:     Impact::WriteFile,
        },
        ServiceImpactModel {
            service_id: rdp.as_ref().unwrap().id.clone().unwrap(),
            impact:     Impact::ExecuteCode,
        },
        ServiceImpactModel {
            service_id: rdp.as_ref().unwrap().id.clone().unwrap(),
            impact:     Impact::ReadFile,
        },
        ServiceImpactModel {
            service_id: rdp.as_ref().unwrap().id.clone().unwrap(),
            impact:     Impact::WriteFile,
        },
        ServiceImpactModel {
            service_id: openvnc.as_ref().unwrap().id.clone().unwrap(),
            impact:     Impact::ExecuteCode,
        },
        ServiceImpactModel {
            service_id: openvnc.as_ref().unwrap().id.clone().unwrap(),
            impact:     Impact::ReadFile,
        },
        ServiceImpactModel {
            service_id: openvnc.as_ref().unwrap().id.clone().unwrap(),
            impact:     Impact::WriteFile,
        },
        ServiceImpactModel {
            service_id: nginx.as_ref().unwrap().id.clone().unwrap(),
            impact:     Impact::ExecuteCode,
        },
        ServiceImpactModel {
            service_id: nginx.as_ref().unwrap().id.clone().unwrap(),
            impact:     Impact::ReadFile,
        },
        ServiceImpactModel {
            service_id: nginx.as_ref().unwrap().id.clone().unwrap(),
            impact:     Impact::WriteFile,
        },
        ServiceImpactModel {
            service_id: apache.as_ref().unwrap().id.clone().unwrap(),
            impact:     Impact::ExecuteCode,
        },
        ServiceImpactModel {
            service_id: apache.as_ref().unwrap().id.clone().unwrap(),
            impact:     Impact::ReadFile,
        },
        ServiceImpactModel {
            service_id: apache.as_ref().unwrap().id.clone().unwrap(),
            impact:     Impact::WriteFile,
        },
        ServiceImpactModel {
            service_id: outlook.as_ref().unwrap().id.clone().unwrap(),
            impact:     Impact::WriteFile,
        },
        ServiceImpactModel {
            service_id: thunderbird.as_ref().unwrap().id.clone().unwrap(),
            impact:     Impact::WriteFile,
        },
        ServiceImpactModel {
            service_id: msoffice.as_ref().unwrap().id.clone().unwrap(),
            impact:     Impact::ExecuteCode,
        },
        ServiceImpactModel {
            service_id: msoffice.as_ref().unwrap().id.clone().unwrap(),
            impact:     Impact::ReadFile,
        },
        ServiceImpactModel {
            service_id: msoffice.as_ref().unwrap().id.clone().unwrap(),
            impact:     Impact::WriteFile,
        },
        ServiceImpactModel {
            service_id: firefox.as_ref().unwrap().id.clone().unwrap(),
            impact:     Impact::ReadFile,
        },
        ServiceImpactModel {
            service_id: firefox.as_ref().unwrap().id.clone().unwrap(),
            impact:     Impact::WriteFile,
        },
        ServiceImpactModel {
            service_id: edge.as_ref().unwrap().id.clone().unwrap(),
            impact:     Impact::ReadFile,
        },
        ServiceImpactModel {
            service_id: edge.as_ref().unwrap().id.clone().unwrap(),
            impact:     Impact::WriteFile,
        },
        ServiceImpactModel {
            service_id: redis.as_ref().unwrap().id.clone().unwrap(),
            impact:     Impact::ReadFile,
        },
        ServiceImpactModel {
            service_id: redis.as_ref().unwrap().id.clone().unwrap(),
            impact:     Impact::WriteFile,
        },
        ServiceImpactModel {
            service_id: mysql.as_ref().unwrap().id.clone().unwrap(),
            impact:     Impact::ReadFile,
        },
        ServiceImpactModel {
            service_id: mysql.as_ref().unwrap().id.clone().unwrap(),
            impact:     Impact::WriteFile,
        },
        ServiceImpactModel {
            service_id: smb.as_ref().unwrap().id.clone().unwrap(),
            impact:     Impact::ReadFile,
        },
        ServiceImpactModel {
            service_id: smb.as_ref().unwrap().id.clone().unwrap(),
            impact:     Impact::WriteFile,
        },
        ServiceImpactModel {
            service_id: ftp.as_ref().unwrap().id.clone().unwrap(),
            impact:     Impact::ReadFile,
        },
        ServiceImpactModel {
            service_id: ftp.as_ref().unwrap().id.clone().unwrap(),
            impact:     Impact::WriteFile,
        },
        ServiceImpactModel {
            service_id: powershell.as_ref().unwrap().id.clone().unwrap(),
            impact:     Impact::ExecuteCode,
        },
        ServiceImpactModel {
            service_id: powershell.as_ref().unwrap().id.clone().unwrap(),
            impact:     Impact::ReadFile,
        },
        ServiceImpactModel {
            service_id: powershell.as_ref().unwrap().id.clone().unwrap(),
            impact:     Impact::WriteFile,
        },
        ServiceImpactModel {
            service_id: bash.as_ref().unwrap().id.clone().unwrap(),
            impact:     Impact::ExecuteCode,
        },
        ServiceImpactModel {
            service_id: bash.as_ref().unwrap().id.clone().unwrap(),
            impact:     Impact::ReadFile,
        },
        ServiceImpactModel {
            service_id: bash.as_ref().unwrap().id.clone().unwrap(),
            impact:     Impact::WriteFile,
        },
        ServiceImpactModel {
            service_id: spoolsv.as_ref().unwrap().id.clone().unwrap(),
            impact:     Impact::ExecuteCode,
        },
        ServiceImpactModel {
            service_id: sudo.as_ref().unwrap().id.clone().unwrap(),
            impact:     Impact::ExecuteCode,
        },
        ServiceImpactModel {
            service_id: telnet.as_ref().unwrap().id.clone().unwrap(),
            impact:     Impact::ExecuteCode,
        },
        ServiceImpactModel {
            service_id: telnet.as_ref().unwrap().id.clone().unwrap(),
            impact:     Impact::ReadFile,
        },
        ServiceImpactModel {
            service_id: telnet.as_ref().unwrap().id.clone().unwrap(),
            impact:     Impact::WriteFile,
        },
        ServiceImpactModel {
            service_id: winrm.as_ref().unwrap().id.clone().unwrap(),
            impact:     Impact::ExecuteCode,
        },
        ServiceImpactModel {
            service_id: winrm.as_ref().unwrap().id.clone().unwrap(),
            impact:     Impact::ReadFile,
        },
        ServiceImpactModel {
            service_id: winrm.as_ref().unwrap().id.clone().unwrap(),
            impact:     Impact::WriteFile,
        },
        ServiceImpactModel {
            service_id: teamviewer.as_ref().unwrap().id.clone().unwrap(),
            impact:     Impact::ExecuteCode,
        },
        ServiceImpactModel {
            service_id: teamviewer.as_ref().unwrap().id.clone().unwrap(),
            impact:     Impact::ReadFile,
        },
        ServiceImpactModel {
            service_id: teamviewer.as_ref().unwrap().id.clone().unwrap(),
            impact:     Impact::WriteFile,
        },
        ServiceImpactModel {
            service_id: wireguard.as_ref().unwrap().id.clone().unwrap(),
            impact:     Impact::UseNetwork,
        },
        ServiceImpactModel {
            service_id: skype.as_ref().unwrap().id.clone().unwrap(),
            impact:     Impact::UseNetwork,
        },
        ServiceImpactModel {
            service_id: custom.as_ref().unwrap().id.clone().unwrap(),
            impact:     Impact::ExecuteCode,
        },
        ServiceImpactModel {
            service_id: custom.as_ref().unwrap().id.clone().unwrap(),
            impact:     Impact::ReadFile,
        },
        ServiceImpactModel {
            service_id: custom.as_ref().unwrap().id.clone().unwrap(),
            impact:     Impact::WriteFile,
        },
        ServiceImpactModel {
            service_id: lightdm.as_ref().unwrap().id.clone().unwrap(),
            impact:     Impact::WriteFile,
        },
        ServiceImpactModel {
            service_id: lightdm.as_ref().unwrap().id.clone().unwrap(),
            impact:     Impact::ReadFile,
        },
        ServiceImpactModel {
            service_id: lightdm.as_ref().unwrap().id.clone().unwrap(),
            impact:     Impact::ExecuteCode,
        },
        ServiceImpactModel {
            service_id: winlogon.as_ref().unwrap().id.clone().unwrap(),
            impact:     Impact::WriteFile,
        },
        ServiceImpactModel {
            service_id: winlogon.as_ref().unwrap().id.clone().unwrap(),
            impact:     Impact::ReadFile,
        },
        ServiceImpactModel {
            service_id: winlogon.as_ref().unwrap().id.clone().unwrap(),
            impact:     Impact::ExecuteCode,
        },
        ServiceImpactModel {
            service_id: docker.as_ref().unwrap().id.clone().unwrap(),
            impact:     Impact::ReadFile,
        },
        ServiceImpactModel {
            service_id: docker.as_ref().unwrap().id.clone().unwrap(),
            impact:     Impact::WriteFile,
        },
    ];

    let func_data_job = Mutations::insert_multiple_functional_data(func_data, db);
    let tags_job = Mutations::insert_multiple_tags(tags, db);
    let impact_job = Mutations::insert_multiple_impacts(impacts, db);

    let (func_res, tag_res, impact_res) = tokio::join!(func_data_job, tags_job, impact_job);
    func_res?;
    tag_res?;
    impact_res?;

    Mutations::insert_enabler(
        EnablerModel {
            id:       0,
            r#type:   Option::from(EnablerType::Cve),
            details:  Option::from(json!("dummy")),
            main_cpe: String::new(),
            config:   None,
        },
        db,
    )
    .await?;

    Ok(())
}

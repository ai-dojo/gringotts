use clap::ValueEnum;
use sea_orm::{DatabaseConnection, DbErr};
use strum_macros::EnumString;

use crate::mock::{carbanak, darkseoul, kcag, nitro, sandworm, stuxnet};

#[derive(EnumString, Clone, Debug, ValueEnum)]
pub enum MockScenarios {
    Kcag,
    Carbanak,
    Nitro,
    DarkSeoul,
    Sandworm,
    Stuxnet,
}

impl MockScenarios {
    pub async fn call(&self, db: &DatabaseConnection) -> Result<(), DbErr> {
        match self {
            Self::Kcag => kcag::mock(db).await,
            Self::Carbanak => carbanak::mock(db).await,
            Self::Nitro => nitro::mock(db).await,
            Self::DarkSeoul => darkseoul::mock(db).await,
            Self::Sandworm => sandworm::mock(db).await,
            Self::Stuxnet => stuxnet::mock(db).await,
        }
    }
}

use sea_orm::{DatabaseConnection, DbErr};
use serde_json::json;
use shared::{
    entity::{
        prelude::{EnablerModel, FunctionalDataModel, ServiceImpactModel, ServiceModel, ServiceTagModel},
        sea_orm_active_enums::{DbPrivilegeLevel, EnablerType, Impact, Tag},
    },
    mutations::Mutations,
};

pub async fn mock(db: &DatabaseConnection) -> Result<(), DbErr> {
    let ssh_job = Mutations::save_service(
        ServiceModel {
            name:    String::from("ssh"),
            vendor:  String::from("OpenSSH"),
            version: String::from("1.0.0"),
            id:      0,
        },
        db,
    );
    let rdp_job = Mutations::save_service(
        ServiceModel {
            name:    String::from("rdp"),
            vendor:  String::from("Microsoft"),
            version: String::from("1.0.0"),
            id:      0,
        },
        db,
    );
    let powershell_job = Mutations::save_service(
        ServiceModel {
            name:    String::from("powershell"),
            vendor:  String::from("Microsoft"),
            version: String::from("1.0.0"),
            id:      0,
        },
        db,
    );
    let custom_job = Mutations::save_service(
        ServiceModel {
            name:    String::from("customapp"),
            vendor:  String::from("we"),
            version: String::from("1.0.0"),
            id:      0,
        },
        db,
    );
    let lsass_job = Mutations::save_service(
        ServiceModel {
            name:    String::from("lsass"),
            vendor:  String::from("Microsoft"),
            version: String::from("1.0.0"),
            id:      0,
        },
        db,
    );
    let apache_job = Mutations::save_service(
        ServiceModel {
            name:    String::from("apache-web"),
            vendor:  String::from("apache"),
            version: String::from("1.0.0"),
            id:      0,
        },
        db,
    );
    let bash_job = Mutations::save_service(
        ServiceModel {
            name:    String::from("bash"),
            vendor:  String::from("Linus"),
            version: String::from("1.0.0"),
            id:      0,
        },
        db,
    );
    let systemd_job = Mutations::save_service(
        ServiceModel {
            name:    String::from("systemd"),
            vendor:  String::from("GNU"),
            version: String::from("1.0.0"),
            id:      0,
        },
        db,
    );
    let tasksched_job = Mutations::save_service(
        ServiceModel {
            name:    String::from("TaskScheduler"),
            vendor:  String::from("Microsoft"),
            version: String::from("1.0.0"),
            id:      0,
        },
        db,
    );
    let chrome_job = Mutations::save_service(
        ServiceModel {
            name:    String::from("chrome"),
            vendor:  String::from("Google"),
            version: String::from("1.0.0"),
            id:      0,
        },
        db,
    );

    let (ssh, rdp, powershell, lsass, customapp, apache, bash, systemd, tasksched, chrome) = tokio::join!(
        ssh_job,
        rdp_job,
        powershell_job,
        lsass_job,
        custom_job,
        apache_job,
        bash_job,
        systemd_job,
        tasksched_job,
        chrome_job
    );

    let func_data = vec![
        FunctionalDataModel {
            id:                0,
            auto_elevation:    Some(true),
            executable_access: Some(DbPrivilegeLevel::Administrator),
            target_software:   Some(String::from("*")),
            service:           ssh.as_ref().unwrap().id.clone().unwrap(),
            ports:             Some(vec![22]),
        },
        FunctionalDataModel {
            id:                0,
            auto_elevation:    Some(true),
            executable_access: Some(DbPrivilegeLevel::Administrator),
            target_software:   Some(String::from("windows")),
            service:           rdp.as_ref().unwrap().id.clone().unwrap(),
            ports:             Some(vec![3389]),
        },
        FunctionalDataModel {
            id:                0,
            auto_elevation:    Some(false),
            executable_access: Some(DbPrivilegeLevel::User),
            target_software:   Some(String::from("windows")),
            service:           powershell.as_ref().unwrap().id.clone().unwrap(),
            ports:             None,
        },
        FunctionalDataModel {
            id:                0,
            auto_elevation:    Some(false),
            executable_access: Some(DbPrivilegeLevel::LocalSystem),
            target_software:   Some(String::from("windows")),
            service:           lsass.as_ref().unwrap().id.clone().unwrap(),
            ports:             None,
        },
        FunctionalDataModel {
            id:                0,
            auto_elevation:    Some(false),
            executable_access: Some(DbPrivilegeLevel::Administrator),
            target_software:   Some(String::from("windows")),
            service:           customapp.as_ref().unwrap().id.clone().unwrap(),
            ports:             Some(vec![8443]),
        },
        FunctionalDataModel {
            id:                0,
            auto_elevation:    Some(false),
            executable_access: Some(DbPrivilegeLevel::Service),
            target_software:   Some(String::from("*")),
            service:           apache.as_ref().unwrap().id.clone().unwrap(),
            ports:             Some(vec![80, 443]),
        },
        FunctionalDataModel {
            id:                0,
            auto_elevation:    Some(false),
            executable_access: Some(DbPrivilegeLevel::User),
            target_software:   Some(String::from("*nix")),
            service:           bash.as_ref().unwrap().id.clone().unwrap(),
            ports:             None,
        },
        FunctionalDataModel {
            id:                0,
            auto_elevation:    Some(true),
            executable_access: Some(DbPrivilegeLevel::LocalSystem),
            target_software:   Some(String::from("*nix")),
            service:           systemd.as_ref().unwrap().id.clone().unwrap(),
            ports:             None,
        },
        FunctionalDataModel {
            id:                0,
            auto_elevation:    Some(false),
            executable_access: Some(DbPrivilegeLevel::User),
            target_software:   Some(String::from("windows")),
            service:           chrome.as_ref().unwrap().id.clone().unwrap(),
            ports:             None,
        },
        FunctionalDataModel {
            id:                0,
            auto_elevation:    Some(true),
            executable_access: Some(DbPrivilegeLevel::Administrator),
            target_software:   Some(String::from("windows")),
            service:           tasksched.as_ref().unwrap().id.clone().unwrap(),
            ports:             None,
        },
    ];

    let tags = vec![
        ServiceTagModel {
            service_id: ssh.as_ref().unwrap().id.clone().unwrap(),
            tag:        Tag::RemoteAccess,
        },
        ServiceTagModel {
            service_id: rdp.as_ref().unwrap().id.clone().unwrap(),
            tag:        Tag::RemoteAccess,
        },
        ServiceTagModel {
            service_id: powershell.as_ref().unwrap().id.clone().unwrap(),
            tag:        Tag::Terminal,
        },
        ServiceTagModel {
            service_id: powershell.as_ref().unwrap().id.clone().unwrap(),
            tag:        Tag::AdministrativeTool,
        },
        ServiceTagModel {
            service_id: lsass.as_ref().unwrap().id.clone().unwrap(),
            tag:        Tag::SystemComponent,
        },
        ServiceTagModel {
            service_id: apache.as_ref().unwrap().id.clone().unwrap(),
            tag:        Tag::WebHosting,
        },
        ServiceTagModel {
            service_id: bash.as_ref().unwrap().id.clone().unwrap(),
            tag:        Tag::Terminal,
        },
        ServiceTagModel {
            service_id: bash.as_ref().unwrap().id.clone().unwrap(),
            tag:        Tag::AdministrativeTool,
        },
        ServiceTagModel {
            service_id: customapp.as_ref().unwrap().id.clone().unwrap(),
            tag:        Tag::App,
        },
        ServiceTagModel {
            service_id: chrome.as_ref().unwrap().id.clone().unwrap(),
            tag:        Tag::WebBrowser,
        },
        ServiceTagModel {
            service_id: systemd.as_ref().unwrap().id.clone().unwrap(),
            tag:        Tag::SystemComponent,
        },
        ServiceTagModel {
            service_id: tasksched.as_ref().unwrap().id.clone().unwrap(),
            tag:        Tag::SystemComponent,
        },
        ServiceTagModel {
            service_id: lsass.as_ref().unwrap().id.clone().unwrap(),
            tag:        Tag::SystemComponent,
        },
    ];

    let impacts = vec![
        ServiceImpactModel {
            service_id: ssh.as_ref().unwrap().id.clone().unwrap(),
            impact:     Impact::ExecuteCode,
        },
        ServiceImpactModel {
            service_id: ssh.as_ref().unwrap().id.clone().unwrap(),
            impact:     Impact::ReadFile,
        },
        ServiceImpactModel {
            service_id: ssh.as_ref().unwrap().id.clone().unwrap(),
            impact:     Impact::WriteFile,
        },
        ServiceImpactModel {
            service_id: powershell.as_ref().unwrap().id.clone().unwrap(),
            impact:     Impact::ExecuteCode,
        },
        ServiceImpactModel {
            service_id: powershell.as_ref().unwrap().id.clone().unwrap(),
            impact:     Impact::ReadFile,
        },
        ServiceImpactModel {
            service_id: powershell.as_ref().unwrap().id.clone().unwrap(),
            impact:     Impact::WriteFile,
        },
        ServiceImpactModel {
            service_id: apache.as_ref().unwrap().id.clone().unwrap(),
            impact:     Impact::ExecuteCode,
        },
        ServiceImpactModel {
            service_id: apache.as_ref().unwrap().id.clone().unwrap(),
            impact:     Impact::ReadFile,
        },
        ServiceImpactModel {
            service_id: apache.as_ref().unwrap().id.clone().unwrap(),
            impact:     Impact::WriteFile,
        },
        ServiceImpactModel {
            service_id: bash.as_ref().unwrap().id.clone().unwrap(),
            impact:     Impact::ExecuteCode,
        },
        ServiceImpactModel {
            service_id: bash.as_ref().unwrap().id.clone().unwrap(),
            impact:     Impact::ReadFile,
        },
        ServiceImpactModel {
            service_id: bash.as_ref().unwrap().id.clone().unwrap(),
            impact:     Impact::WriteFile,
        },
        ServiceImpactModel {
            service_id: customapp.as_ref().unwrap().id.clone().unwrap(),
            impact:     Impact::ExecuteCode,
        },
        ServiceImpactModel {
            service_id: customapp.as_ref().unwrap().id.clone().unwrap(),
            impact:     Impact::ReadFile,
        },
        ServiceImpactModel {
            service_id: customapp.as_ref().unwrap().id.clone().unwrap(),
            impact:     Impact::WriteFile,
        },
        ServiceImpactModel {
            service_id: chrome.as_ref().unwrap().id.clone().unwrap(),
            impact:     Impact::ReadFile,
        },
        ServiceImpactModel {
            service_id: chrome.as_ref().unwrap().id.clone().unwrap(),
            impact:     Impact::WriteFile,
        },
        ServiceImpactModel {
            service_id: systemd.as_ref().unwrap().id.clone().unwrap(),
            impact:     Impact::ExecuteCode,
        },
        ServiceImpactModel {
            service_id: tasksched.as_ref().unwrap().id.clone().unwrap(),
            impact:     Impact::ExecuteCode,
        },
    ];

    let func_data_job = Mutations::insert_multiple_functional_data(func_data, db);
    let tags_job = Mutations::insert_multiple_tags(tags, db);
    let impact_job = Mutations::insert_multiple_impacts(impacts, db);

    let (func_res, tag_res, impact_res) = tokio::join!(func_data_job, tags_job, impact_job);
    func_res?;
    tag_res?;
    impact_res?;

    Mutations::insert_enabler(
        EnablerModel {
            id:       0,
            r#type:   Option::from(EnablerType::Cve),
            details:  Option::from(json!("dummy")),
            main_cpe: String::new(),
            config:   None,
        },
        db,
    )
    .await?;

    Ok(())
}

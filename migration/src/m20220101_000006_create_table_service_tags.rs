use sea_orm_migration::{
    prelude::*,
    sea_orm::{EnumIter, Iterable},
    sea_query::extension::postgres::TypeCreateStatement,
};

use crate::m20220101_000005_create_table_service::Services;

#[derive(DeriveMigrationName)]
pub struct Migration;

#[async_trait::async_trait]
impl MigrationTrait for Migration {
    async fn up(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        manager
            .create_type(
                TypeCreateStatement::new()
                    .as_enum(Tag::Table)
                    .values(Tag::iter().skip(1))
                    .to_owned(),
            )
            .await?;

        manager
            .create_table(
                TableCreateStatement::new()
                    .table(ServiceTags::Table)
                    .if_not_exists()
                    .col(ColumnDef::new(ServiceTags::ServiceId).integer())
                    .col(ColumnDef::new(ServiceTags::Tag).enumeration(Tag::Table, Tag::iter().skip(1)))
                    .primary_key(Index::create().col(ServiceTags::ServiceId).col(ServiceTags::Tag))
                    .to_owned(),
            )
            .await?;

        manager
            .create_foreign_key(
                ForeignKeyCreateStatement::new()
                    .name("ServiceFK")
                    .from_tbl(ServiceTags::Table)
                    .from_col(ServiceTags::ServiceId)
                    .to_tbl(Services::Table)
                    .to_col(Services::Id)
                    .on_update(ForeignKeyAction::Cascade)
                    .on_delete(ForeignKeyAction::Cascade)
                    .to_owned(),
            )
            .await
    }

    async fn down(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        manager
            .drop_table(Table::drop().table(ServiceTags::Table).to_owned())
            .await?;

        manager.drop_table(Table::drop().table(Tag::Table).to_owned()).await
    }
}

#[derive(DeriveIden, EnumIter)]
pub enum Tag {
    Table,
    AdministrativeTool,
    FinanceSoftware,
    DevelopmentTool,
    VirtualizationPlatform,
    RemoteAccess,
    OfficeApplication,
    Media,
    Teleconference,
    WebBrowser,
    FileBrowser,
    Archiver,
    Antivirus,
    PasswordManager,
    Messenger,
    Terminal,
    SystemComponent,
    WebHosting,
    EmailClient,
    Database,
    FileSharing,
    App,
}

#[derive(DeriveIden)]
pub enum ServiceTags {
    Table,
    ServiceId,
    Tag,
}

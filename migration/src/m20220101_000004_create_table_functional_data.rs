use sea_orm::EnumIter;
use sea_orm_migration::{prelude::*, sea_orm::Iterable, sea_query::extension::postgres::TypeCreateStatement};

use crate::m20220101_000005_create_table_service::Services;

#[derive(DeriveMigrationName)]
pub struct Migration;

#[async_trait::async_trait]
impl MigrationTrait for Migration {
    async fn up(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        manager
            .create_type(
                TypeCreateStatement::new()
                    .as_enum(DbPrivilegeLevel::Table)
                    .values(DbPrivilegeLevel::iter().skip(1))
                    .to_owned(),
            )
            .await?;
        manager
            .create_table(
                Table::create()
                    .table(FunctionalData::Table)
                    .if_not_exists()
                    .col(
                        ColumnDef::new(FunctionalData::Id)
                            .integer()
                            .not_null()
                            .auto_increment()
                            .primary_key(),
                    )
                    .col(ColumnDef::new(FunctionalData::AutoElevation).boolean())
                    .col(
                        ColumnDef::new(FunctionalData::ExecutableAccess)
                            .enumeration(DbPrivilegeLevel::Table, DbPrivilegeLevel::iter().skip(1)),
                    )
                    .col(ColumnDef::new(FunctionalData::TargetSoftware).string())
                    .col(ColumnDef::new(FunctionalData::Service).integer().not_null())
                    .col(ColumnDef::new(FunctionalData::Ports).array(ColumnType::Integer))
                    .to_owned(),
            )
            .await?;

        manager
            .create_foreign_key(
                ForeignKey::create()
                    .name("serviceFK")
                    .from_tbl(FunctionalData::Table)
                    .from_col(FunctionalData::Service)
                    .to_tbl(Services::Table)
                    .to_col(Services::Id)
                    .on_update(ForeignKeyAction::Cascade)
                    .on_delete(ForeignKeyAction::Cascade)
                    .to_owned(),
            )
            .await
    }

    async fn down(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        manager
            .drop_table(Table::drop().table(FunctionalData::Table).to_owned())
            .await?;
        manager
            .drop_table(Table::drop().table(DbPrivilegeLevel::Table).to_owned())
            .await
    }
}

#[derive(EnumIter, DeriveIden)]
enum DbPrivilegeLevel {
    Table,
    None,
    Service,
    User,
    Administrator,
    LocalSystem,
}

#[derive(DeriveIden)]
enum FunctionalData {
    Table,
    Id,
    AutoElevation,
    ExecutableAccess,
    TargetSoftware,
    Service,
    Ports,
}

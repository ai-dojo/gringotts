use sea_orm_migration::{
    prelude::{extension::postgres::TypeCreateStatement, *},
    sea_orm::{EnumIter, Iterable},
};

use crate::{
    m20220101_000005_create_table_service::Services,
    m20220101_000008_create_table_actions::Actions,
    m20220101_000009_create_table_enablers::Enablers,
};

#[derive(DeriveMigrationName)]
pub struct Migration;

#[async_trait::async_trait]
impl MigrationTrait for Migration {
    async fn up(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        manager
            .create_type(
                TypeCreateStatement::new()
                    .as_enum(ActionGuard::Table)
                    .values(ActionGuard::iter().skip(1))
                    .to_owned(),
            )
            .await?;

        manager
            .create_table(
                TableCreateStatement::new()
                    .table(ApplicabilityMatrix::Table)
                    .if_not_exists()
                    .col(ColumnDef::new(ApplicabilityMatrix::ServiceId).integer())
                    .col(ColumnDef::new(ApplicabilityMatrix::ActionId).integer())
                    .col(ColumnDef::new(ApplicabilityMatrix::EnablerId).integer())
                    .col(
                        ColumnDef::new(ApplicabilityMatrix::Guard)
                            .enumeration(ActionGuard::Table, ActionGuard::iter().skip(1)),
                    )
                    .primary_key(
                        Index::create()
                            .col(ApplicabilityMatrix::ServiceId)
                            .col(ApplicabilityMatrix::ActionId)
                            .col(ApplicabilityMatrix::EnablerId),
                    )
                    .to_owned(),
            )
            .await?;

        manager
            .create_foreign_key(
                ForeignKeyCreateStatement::new()
                    .name("ServiceFK")
                    .from_tbl(ApplicabilityMatrix::Table)
                    .from_col(ApplicabilityMatrix::ServiceId)
                    .to_tbl(Services::Table)
                    .to_col(Services::Id)
                    .on_update(ForeignKeyAction::Cascade)
                    .on_delete(ForeignKeyAction::Cascade)
                    .to_owned(),
            )
            .await?;

        manager
            .create_foreign_key(
                ForeignKeyCreateStatement::new()
                    .name("ActionFK")
                    .from_tbl(ApplicabilityMatrix::Table)
                    .from_col(ApplicabilityMatrix::ActionId)
                    .to_tbl(Actions::Table)
                    .to_col(Actions::Id)
                    .on_update(ForeignKeyAction::Cascade)
                    .on_delete(ForeignKeyAction::Cascade)
                    .to_owned(),
            )
            .await?;

        manager
            .create_foreign_key(
                ForeignKeyCreateStatement::new()
                    .name("EnablerFK")
                    .from_tbl(ApplicabilityMatrix::Table)
                    .from_col(ApplicabilityMatrix::EnablerId)
                    .to_tbl(Enablers::Table)
                    .to_col(Enablers::Id)
                    .on_update(ForeignKeyAction::Cascade)
                    .on_delete(ForeignKeyAction::Cascade)
                    .to_owned(),
            )
            .await
    }

    async fn down(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        manager
            .drop_table(Table::drop().table(ApplicabilityMatrix::Table).to_owned())
            .await
    }
}

#[derive(DeriveIden)]
pub enum ApplicabilityMatrix {
    Table,
    ServiceId,
    ActionId,
    EnablerId,
    Guard,
}

#[derive(DeriveIden, EnumIter)]
pub enum ActionGuard {
    Table,
    AlwaysUsable,
    TargetMustBeProcess,
    TargetMustBeExecutable,
}

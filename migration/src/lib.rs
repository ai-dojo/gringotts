#![allow(clippy::multiple_crate_versions)]
pub use sea_orm_migration::prelude::*;

mod m20220101_000001_create_table_deployment_data;
mod m20220101_000002_create_table_cpe;
mod m20220101_000003_create_table_origin_data;
mod m20220101_000004_create_table_functional_data;
mod m20220101_000005_create_table_service;
mod m20220101_000006_create_table_service_tags;
mod m20220101_000007_create_table_service_impacts;
mod m20220101_000008_create_table_actions;
mod m20220101_000009_create_table_enablers;
mod m20220101_000010_create_table_enabler_impacts;
mod m20220101_000011_create_table_service_action_enabler_applicability;

pub struct Migrator;

#[async_trait::async_trait]
impl MigratorTrait for Migrator {
    fn migrations() -> Vec<Box<dyn MigrationTrait>> {
        vec![
            Box::new(m20220101_000005_create_table_service::Migration),
            Box::new(m20220101_000001_create_table_deployment_data::Migration),
            Box::new(m20220101_000002_create_table_cpe::Migration),
            Box::new(m20220101_000003_create_table_origin_data::Migration),
            Box::new(m20220101_000004_create_table_functional_data::Migration),
            Box::new(m20220101_000006_create_table_service_tags::Migration),
            Box::new(m20220101_000007_create_table_service_impacts::Migration),
            Box::new(m20220101_000008_create_table_actions::Migration),
            Box::new(m20220101_000009_create_table_enablers::Migration),
            Box::new(m20220101_000010_create_table_enabler_impacts::Migration),
            Box::new(m20220101_000011_create_table_service_action_enabler_applicability::Migration),
        ]
    }
}

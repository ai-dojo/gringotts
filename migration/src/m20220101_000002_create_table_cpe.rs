use sea_orm_migration::prelude::*;

use super::m20220101_000005_create_table_service::Services;

#[derive(DeriveMigrationName)]
pub struct Migration;

#[async_trait::async_trait]
impl MigrationTrait for Migration {
    async fn up(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        manager
            .create_table(
                Table::create()
                    .table(Cpe::Table)
                    .if_not_exists()
                    .col(
                        ColumnDef::new(Cpe::Id)
                            .integer()
                            .not_null()
                            .auto_increment()
                            .primary_key(),
                    )
                    .col(ColumnDef::new(Cpe::CpeName).string().not_null())
                    .col(ColumnDef::new(Cpe::Created).date_time().not_null())
                    .col(ColumnDef::new(Cpe::LastModified).date_time().not_null())
                    .col(ColumnDef::new(Cpe::Service).integer().not_null())
                    .to_owned(),
            )
            .await?;

        manager
            .create_foreign_key(
                ForeignKey::create()
                    .name("serviceFK")
                    .from_tbl(Cpe::Table)
                    .from_col(Cpe::Service)
                    .to_tbl(Services::Table)
                    .to_col(Services::Id)
                    .on_update(ForeignKeyAction::Cascade)
                    .on_delete(ForeignKeyAction::Cascade)
                    .to_owned(),
            )
            .await
    }

    async fn down(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        manager.drop_table(Table::drop().table(Cpe::Table).to_owned()).await
    }
}

#[derive(DeriveIden)]
#[allow(clippy::enum_variant_names)]
enum Cpe {
    Table,
    Id,
    CpeName,
    Created,
    LastModified,
    Service,
}

use sea_orm_migration::{
    prelude::*,
    sea_orm::{EnumIter, Iterable},
    sea_query::extension::postgres::TypeCreateStatement,
};

use crate::m20220101_000005_create_table_service::Services;

#[derive(DeriveMigrationName)]
pub struct Migration;

#[async_trait::async_trait]
impl MigrationTrait for Migration {
    async fn up(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        manager
            .create_type(
                TypeCreateStatement::new()
                    .as_enum(Impact::Table)
                    .values(Impact::iter().skip(1))
                    .to_owned(),
            )
            .await?;

        manager
            .create_table(
                TableCreateStatement::new()
                    .table(ServiceImpacts::Table)
                    .if_not_exists()
                    .col(ColumnDef::new(ServiceImpacts::ServiceId).integer())
                    .col(ColumnDef::new(ServiceImpacts::Impact).enumeration(Impact::Table, Impact::iter().skip(1)))
                    .primary_key(
                        Index::create()
                            .col(ServiceImpacts::ServiceId)
                            .col(ServiceImpacts::Impact),
                    )
                    .to_owned(),
            )
            .await?;

        manager
            .create_foreign_key(
                ForeignKeyCreateStatement::new()
                    .name("ServiceFK")
                    .from_tbl(ServiceImpacts::Table)
                    .from_col(ServiceImpacts::ServiceId)
                    .to_tbl(Services::Table)
                    .to_col(Services::Id)
                    .on_update(ForeignKeyAction::Cascade)
                    .on_delete(ForeignKeyAction::Cascade)
                    .to_owned(),
            )
            .await
    }

    async fn down(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        manager
            .drop_table(Table::drop().table(ServiceImpacts::Table).to_owned())
            .await?;

        manager.drop_table(Table::drop().table(Impact::Table).to_owned()).await
    }
}

#[derive(DeriveIden, EnumIter)]
pub enum Impact {
    Table,
    ReadFile,
    WriteFile,
    ExecuteCode,
    UseNetwork,
    /* TODO: add more */
}

#[derive(DeriveIden)]
pub enum ServiceImpacts {
    Table,
    ServiceId,
    Impact,
}

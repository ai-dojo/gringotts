use sea_orm_migration::{
    prelude::*,
    sea_orm::{EnumIter, Iterable},
    sea_query::extension::postgres::TypeCreateStatement,
};

use crate::m20220101_000009_create_table_enablers::Enablers;

#[derive(DeriveMigrationName)]
pub struct Migration;

#[async_trait::async_trait]
impl MigrationTrait for Migration {
    async fn up(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        manager
            .create_type(
                TypeCreateStatement::new()
                    .as_enum(EnablerImpactType::Table)
                    .values(EnablerImpactType::iter().skip(1))
                    .to_owned(),
            )
            .await?;

        manager
            .create_table(
                TableCreateStatement::new()
                    .table(EnablerImpacts::Table)
                    .if_not_exists()
                    .col(ColumnDef::new(EnablerImpacts::EnablerId).integer())
                    .col(
                        ColumnDef::new(EnablerImpacts::Impact)
                            .enumeration(EnablerImpactType::Table, EnablerImpactType::iter().skip(1)),
                    )
                    .primary_key(
                        Index::create()
                            .col(EnablerImpacts::EnablerId)
                            .col(EnablerImpacts::Impact),
                    )
                    .to_owned(),
            )
            .await?;

        manager
            .create_foreign_key(
                ForeignKeyCreateStatement::new()
                    .name("EnablerFK")
                    .from_tbl(EnablerImpacts::Table)
                    .from_col(EnablerImpacts::EnablerId)
                    .to_tbl(Enablers::Table)
                    .to_col(Enablers::Id)
                    .on_update(ForeignKeyAction::Cascade)
                    .on_delete(ForeignKeyAction::Cascade)
                    .to_owned(),
            )
            .await
    }

    async fn down(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        manager
            .drop_table(Table::drop().table(EnablerImpacts::Table).to_owned())
            .await?;

        manager
            .drop_table(Table::drop().table(EnablerImpactType::Table).to_owned())
            .await
    }
}

#[derive(DeriveIden, EnumIter)]
pub enum EnablerImpactType {
    Table,
    DataDisclosure,
    DataManipulation,
    CredentialLeak,
    AuthManipulation,
    AvailabilityViolation,
    MachineConfigurationTampering,
    ArbitraryCodeExecution,
    Allow,
}

#[derive(DeriveIden)]
pub enum EnablerImpacts {
    Table,
    EnablerId,
    Impact,
}

### Creating new release

Release pipeline is triggered with tag like `v1.0`.
```
git tag -a v1.0 -m "New release"
git push origin v1.0
```
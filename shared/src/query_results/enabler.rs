use sea_orm::FromQueryResult;

use crate::entity::sea_orm_active_enums::{EnablerImpactType, EnablerType};

/**
Enabler representation for database queries.
 **/
#[derive(serde::Serialize, serde::Deserialize, FromQueryResult)]
pub struct Enabler {
    pub id:          i32,
    pub kind:        EnablerType,
    pub description: String,
    pub services:    Vec<i32>,
    pub actions:     Vec<i32>,
    // locality:    EnablerLocality, TODO: add this to the DB
    pub impacts:     Vec<EnablerImpactType>,
}

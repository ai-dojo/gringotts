use sea_orm::FromQueryResult;

use crate::entity::sea_orm_active_enums::{DbPrivilegeLevel, Impact, Tag};

/**
Complex service representation for database queries.
 **/
#[derive(Debug, FromQueryResult, serde::Deserialize, serde::Serialize)]
pub struct Service {
    pub id:                i32,
    pub name:              String,
    pub version:           String,
    pub vendor:            String,
    pub auto_elevation:    Option<bool>,
    pub executable_access: Option<DbPrivilegeLevel>,
    pub target_software:   Option<String>,
    pub ports:             Option<Vec<i32>>,
    pub tags:              Vec<Tag>,
    pub impacts:           Vec<Impact>,
}

#![allow(clippy::module_name_repetitions)]

use std::{
    borrow::Cow,
    fmt::{Display, Formatter},
};

#[derive(Debug)]
pub enum GringottsError {
    Misc(Cow<'static, str>),
    SerdeJsonError(serde_json::error::Error),
    SeaOrmDbError(sea_orm::error::DbErr),
    StrumParse(strum::ParseError),
    SerdeError(Cow<'static, str>),
}
impl From<serde_json::error::Error> for GringottsError {
    fn from(value: serde_json::error::Error) -> Self { Self::SerdeJsonError(value) }
}

impl From<sea_orm::error::DbErr> for GringottsError {
    fn from(value: sea_orm::error::DbErr) -> Self { Self::SeaOrmDbError(value) }
}

impl From<strum::ParseError> for GringottsError {
    fn from(value: strum::ParseError) -> Self { Self::StrumParse(value) }
}

impl From<Box<dyn std::error::Error>> for GringottsError {
    fn from(value: Box<dyn std::error::Error>) -> Self { Self::SerdeError(value.to_string().into()) }
}

impl Display for GringottsError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::Misc(val) => {
                write!(f, "{val}")
            }
            Self::SerdeJsonError(e) => {
                write!(f, "{e}")
            }
            Self::SeaOrmDbError(e) => {
                write!(f, "{e}")
            }
            Self::StrumParse(e) => {
                write!(f, "{e}")
            }
            Self::SerdeError(e) => {
                write!(f, "{e}")
            }
        }
    }
}
impl std::error::Error for GringottsError {}
unsafe impl Send for GringottsError {}
unsafe impl Sync for GringottsError {}

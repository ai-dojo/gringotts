#![allow(clippy::multiple_crate_versions)]
pub mod entity;
mod error;
mod types;

pub mod mutations;

pub mod queries;
mod query_results;

pub use entity::prelude::ApplicabilityMatrixModel;
pub use error::GringottsError;
pub use query_results::{enabler::Enabler, service::Service};
pub use types::GringottsResult;

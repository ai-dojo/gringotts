use sea_orm::FromQueryResult;
use serde;
use serde_json::value::Value as Json;

use crate::entity::sea_orm_active_enums::{ActionGuard, EnablerType};

#[derive(FromQueryResult, serde::Deserialize, serde::Serialize, Debug)]
pub struct ActionApplicability {
    pub service_id:          i32,
    pub action_id:           i32,
    pub enabler_id:          i32,
    pub action_description:  Json,
    pub enabler_description: Json,
    pub enabler_type:        EnablerType,
    pub guard:               ActionGuard,
}

#[derive(FromQueryResult, serde::Deserialize, serde::Serialize, Debug)]
pub struct PrimaryKeyView {
    pub id: i32,
}

use crate::GringottsError;

pub type GringottsResult<T> = Result<T, GringottsError>;
pub type ServiceHandle = i32;
pub type ActionHandle = i32;
